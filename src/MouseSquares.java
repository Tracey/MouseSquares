import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Iterator;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

/**
 * This class paints colored squares to a panel and implements an undo and redo
 * feature
 * 
 * @author tyler_tracey
 * @version 1.1.1
 *
 */
@SuppressWarnings("serial")
public class MouseSquares extends JFrame {

    private Stack<Command> undoStack = new Stack<Command>();
    private Stack<Command> redoStack = new Stack<Command>();
    private Stack<ColoredSquare> allSquares = new Stack<ColoredSquare>();

    // JFrame menu bar initialization
    private JMenuBar menuBar = new JMenuBar();
    private JMenu program = new JMenu("Program");
    private JMenu edit = new JMenu("Edit");
    private JMenuItem exit = new JMenuItem("Exit");
    private JMenuItem undo = new JMenuItem("Undo");
    private JMenuItem redo = new JMenuItem("Redo");

    /**
     * This class is the JPanel constructor
     * 
     * @author tyler_tracey
     * @version 1.1.1
     *
     */
    public class MouseSquaresPanel extends JPanel implements MouseListener {

        private ColoredSquare square;

        /**
         * This is the MouseSquaresPanel constructor
         */
        public MouseSquaresPanel() {
            addMouseListener(this);
        }

        /**
         * This method paints the square object to the panel
         */
        @Override
        protected void paintComponent(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
            super.paintComponent(g2);

            Iterator<ColoredSquare> iterator = allSquares.iterator();
            while (iterator.hasNext()) {
                ColoredSquare hold = iterator.next();
                hold.paintMe(g2);
            }
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            // the location of the click
            Point point = new Point(e.getX(), e.getY());
            square = new ColoredSquare(point);
            Com command = new Com(square);
            command.execute();
            undoStack.push(command);
            redoStack.clear();
            undo.setEnabled(true);
            redo.setEnabled(false);
            repaint();
        }

        @Override
        public void mousePressed(MouseEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public void mouseReleased(MouseEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public void mouseEntered(MouseEvent e) {
            // TODO Auto-generated method stub

        }

        @Override
        public void mouseExited(MouseEvent e) {
            // TODO Auto-generated method stub

        }
    }

    /**
     * This is the MouseSquares constructor
     */
    private MouseSquares() {

        super("MouseSquares");
        setSize(500, 500);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        MouseSquaresPanel panel = new MouseSquaresPanel();
        add(panel);

        // Adding JMenu items and bar to frame
        setJMenuBar(menuBar);
        menuBar.add(program);
        program.add(exit);
        menuBar.add(edit);
        edit.add(undo);
        edit.add(redo);

        // Set initially disabled until action can be performed
        undo.setEnabled(false);
        redo.setEnabled(false);

        // Exit action listener
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        // Undo action listener
        undo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Stack<Command> u = getUndo();
                Command c = u.pop();
                c.undo();
                redoStack.push(c);
                redo.setEnabled(true);
                if (undoStack.isEmpty()) {
                    undo.setEnabled(false);
                }
                redo.setEnabled(true);
                repaint();
            }
        });

        // Redo action listener
        redo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Stack<Command> r = getRedo();
                Command c = r.pop();
                c.execute();
                undoStack.push(c);
                if (getRedo().isEmpty()) {
                    redo.setEnabled(false);
                }
                undo.setEnabled(true);
                repaint();
            }
        });

    }

    /**
     * This method returns the colored squares stack of colored squares
     * 
     * @return all squares stack
     */
    public Stack<ColoredSquare> getSquares() {
        return allSquares;
    }

    /**
     * This method returns the undo stack of commands
     * 
     * @return undo stack
     */
    public Stack<Command> getUndo() {
        return undoStack;
    }

    /**
     * This method returns the redo stack of commands
     * 
     * @return redo stack
     */
    public Stack<Command> getRedo() {
        return redoStack;
    }

    /**
     * Main method
     * 
     * @param args
     */
    public static void main(String[] args) {
        JFrame f = new MouseSquares();
        f.setVisible(true);
    }

    /**
     * This class creates an object instance of the colored square being added
     * to to colored squares stack. It implements methods that will push the
     * square into the colored squares stack, and pop the square from the
     * colored squares stack
     * 
     * @author tyler_tracey
     * @version 1.1.1
     *
     */
    private class Com implements Command {

        ColoredSquare sq;

        private Com(ColoredSquare in) {
            sq = in;
        }

        /**
         * This method pushes the colored square to the colored square stack
         */
        @Override
        public Command execute() {
            allSquares.push(sq);
            return this;
        }

        /**
         * This method pops the colored square from the colored square stack
         */
        @Override
        public Command undo() {
            allSquares.pop();
            return this;
        }

    }
}